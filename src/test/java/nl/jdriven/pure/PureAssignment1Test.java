package nl.jdriven.pure;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PureAssignment1Test {

  @Test
  public void testDateString(){
    //Find a way to test if the formatting of this function is done right without reimplementing the function in the test
    String result = new PureAssignment1().dateString(); //TODO find a way to test this method
  }

  @Test
  public void testFlow(){
    //We are not interested in testing databases here, so how do we test businesslogic?
    new PureAssignment1().flow(); //TODO, how to test the businesslogic of this function
  }

}