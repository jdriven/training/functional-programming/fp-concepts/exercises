package nl.jdriven.optional;

import javax.annotation.Nullable;
import java.util.Random;

/**
 * This class has a part 1, you can find it in  /test/nl.jdriven.optional/OptionalAssignment1
 */
public class OptionalAssignment2 {

  /**
      Rewrite this to use Optionals instead of using null.
      You should be able to replace all @Nullable by @Nonnull
      Method signatures will be changed because of this.
      It is best to work from top to bottom, so first solve getPerson, then toUpperCase etc..
   */
  public void flow(){
    Person person = getPerson();
    Person uppercaseName = toUpperCase(person);
    Beer beer = orderBeer(uppercaseName);
    drinkBeer(beer);
  }


  /**
   * Instead of returning null, try to return a empty Optional
   * @return
   */
  @Nullable
  public Person getPerson() {
    //Simulate this sometimes returns null
    Random random = new Random();
    if(random.nextInt(10) > 5){
      return null;
    }
    return new Person("myName", 10);
  }

  /**
   * Is there a way we can use the Optional so we don't have to check for null here?
   * @param p
   * @return
   */
  @Nullable
  public Person toUpperCase(@Nullable Person p){
    if(p == null) {
      return null;
    }
    return new Person(p.name.toUpperCase(), p.age);
  }


  @Nullable
  public Beer orderBeer(@Nullable Person person){
    //Do we still need the null check? and what to return if the person < 18 if we cannot return null?
    if(person == null || person.age < 18){
      return null;
    }
    return new Beer("");
  }

  /**
   * Could we replace this function using build-in things in Optional
   * @param beer
   */
  public void drinkBeer(@Nullable Beer beer){
    if(beer == null){
      System.out.println("No beer present");
    } else {
      System.out.println("Drinking beer: " + beer.name);
    }
  }

  record Person(String name, int age){ }
  record Beer(String name){}
}
