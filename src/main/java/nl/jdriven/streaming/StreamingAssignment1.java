package nl.jdriven.streaming;

import java.util.ArrayList;
import java.util.List;

public class StreamingAssignment1 {

  //Try rewriting this function to use the streaming api with the .map() function
  public List<String> transformToUppercase(List<String> source){
    List<String> result = new ArrayList<>();
    for(String item : source){
      result.add(item.toUpperCase());
    }
    return result;
  }

  //Try rewriting this function to use the streaming api with the .filter() function
  public List<String> removeUppercase(List<String> source){
    List<String> result = new ArrayList<>();
    for(String item : source){
      if(item.equals(item.toLowerCase())){
        result.add(item);
      }
    }
    return result;
  }

  //Try rewriting this function to use the streaming api with both map and filter
  public List<String> flow(List<String> source){
    List<String> result = new ArrayList<>();
    for(String item : source){
      if(item.equals(item.toLowerCase()) && item.length() < 5){
        result.add(item.toUpperCase());
      }
    }
    return result;
  }

  //Try rewriting this function to use the streaming api with both map and filter
  private List<String> flow2(List<Person> persons) {
    List<String> result = new ArrayList<>();

    for (Person p : persons) {
      for(Person sibling : p.siblings()){
        if(sibling.gender().equals("M")) {
          if (sibling.age() > 18 && sibling.age() <= 65) {
            if (sibling.name() != null && sibling.name().startsWith("B")) {
              result.add(sibling.name());
            }
          }
        }
      }
    }
    return result;
  }

  record Person(String name, int age, String gender, List<Person> siblings) {
  }

}
