package nl.jdriven.pure;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

/**
 * The methods in this class are quite hard to test
 * Find a way to test them in the src/test/nl/jdriven/pure/PureAssignment1Test class
 * You will have to change the method signatures for this to work or add new functions
 */
public class PureAssignment1 {

  /**
   * How to test if this function does correct dateformatting?
   * Try to identify the side effect that makes this function hard to test
   * Could we also supply this as a parameter?
   * @return
   */
  public String dateString() {
    return DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now());
  }

  /**
   * We want to test the businesslogic in this function.
   * The database calls make it hard to test or to re-use the businesslogic
   * Can we extract businesslogic to a pure function?
   */
  public void flow(){
      String input = getFromDatabase();
      //start businesslogic (we want to test this bit)
      String afterBusinessLogic = input.toUpperCase();
      //end businesslogic
      storeInDatabase(afterBusinessLogic);
  }

  private String getFromDatabase(){
    return Integer.toString(new Random().nextInt());
  }

  private void storeInDatabase(String input){

  }

}
