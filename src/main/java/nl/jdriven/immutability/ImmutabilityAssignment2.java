package nl.jdriven.immutability;

/**
 * In this assignment we will learn how to change values of an immutable object
 */
public class ImmutabilityAssignment2 {

  /**
   * Implement a method called changeAge so you can properly assign the variable agedChangePerson
   * Try to do this without changing the Person record
   */
  public void flow(){
      Person p = new Person("oldName", 20);
      Person agedChangePerson = null;
  }

  public Person changeAge(Person p, int newAge){
    //TODO
    return null;
  }

  record Person(String name, int age){}

}
