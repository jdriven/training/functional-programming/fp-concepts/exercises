package nl.jdriven.immutability;

import javax.annotation.Nullable;

/**
 * If you start using the immutability pattern you might encounter the situation that some fields are sometimes nullable
 * In this case we have a Contract that needs to be signed before it can be published.
 * Note here that contract is a mutable object.
 * Right now the only thing protecting a possible nullpointer exception in the publishSignedContract is because we didn't forget to sign the contract.
 * Also note that the contract is in an invalid state as long as it is not signed, or can be put into an invalid state later on by calling a setter.
 * Try to think of a way to solve this issue without relying on nullable fields or setters
 *
 * Hint: There is a state transition going on from a Contract to a SignedContract, you can model this using 2 classes instead of just having the Contract
 */
public class ImmutabilityAssignment3 {

  public void flow(){
    Contract contract = getContract();
    signContract(contract);
    publishSignedContract(contract);
  }

  public Contract getContract(){
    return new Contract("content");
  }

  //This should return a SignedContract
  public void signContract(Contract contract){
     contract.setSignature("signed");
  }

  //This should receive a SignedContract
  public void publishSignedContract(Contract contract){
    System.out.println("Published my signed contract with signature " + contract.getSignature().toUpperCase());
  }

  // There should be a second class SignedContract so Contract does not need the signature
  class Contract {
    private String content;
    private String signature;

    public Contract(String content) {
      this.content = content;
    }

    public Contract(String content, @Nullable String signature) {
      this.content = content;
      this.signature = signature;
    }

    public String getContent() {
      return content;
    }

    public void setContent(String content) {
      this.content = content;
    }

    @Nullable
    public String getSignature() {
      return signature;
    }

    public void setSignature(@Nullable String signature) {
      this.signature = signature;
    }
  }

}
