package nl.jdriven.optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Nullable;
import java.util.Optional;

class OptionalAssignment1 {

  /**
   * This method tries to print something if the optional is not empty by doing an .isPresent() check
   * Replace this check using .orElse() to give `result` a new value
   */
  @Test
  public void testOrElse(){
    Optional<String> optional = Optional.ofNullable(OptionalTestHelper.getNullableString());

    String result;
    if(optional.isPresent()){
      result = optional.get();
    } else {
      result = "Optional was empty";
    }
    System.out.println(result);
    Assertions.assertEquals("i could be null", result);
  }

  /**
   * We create an optional here that is NOT empty, so the orElse branch should not run.
   * However, it does print "Returning non-null string" before printing the correct result.
   * This is because the .orElse() condition is eager (it will always be executed regardless of the optional is empty or not)
   * Use .orElseGet() instead to prevent this.
   */
  @Test
  public void testOrElseGet(){
    Optional<String> optional = Optional.of("i am not null");

    String result = optional.orElse(OptionalTestHelper.getNonNullString());
    System.out.println(result);
    Assertions.assertEquals("i am not null", result);
  }

  /**
   * This method gets a nullable string and prints the uppercase version if it is non-null.
   * Put the nullable string into an Optional by using the Optional.ofNullable(nullableString) method.
   * Solve this without using if and using the .map() and .orElse() method instead.
   */
  @Test
  public void testMap(){
    String nullableString = OptionalTestHelper.getNullableString();
    String result = "";
    if(nullableString != null){
      result = nullableString.toUpperCase();
    } else {
      result = "";
    }
    System.out.println(result);
    Assertions.assertEquals(result, "I COULD BE NULL");
  }

  /**
   * This method gets a nullable string and prints the lowercase version if it is non-null.
   * For this it uses a helper method which returns an optional which means we need to convert that to a value using .orElse() in order to use this.
   * Solve this without using if and without using .orElse("") in the OptionalTestHelper.toLowerCase(nullableString).orElse("") statement.
   * You will notice a .map() will give you an Optional<Optional<String>>, try using .flatMap() instead.
   * Flatmap 'flattens', which in this case means that unlike .map() it will not wrap the result into a new Optional.
   */
  @Test
  public void testFlatMap(){
    String nullableString = OptionalTestHelper.getNullableString();
    String result = "";
    if(nullableString != null){
      result = OptionalTestHelper.toLowerCase(nullableString).orElse("");
    } else {
      result = "";
    }
    Assertions.assertEquals("i could be null", result);
  }

  /**
   * Optional also has an 'if' construction which we can use called .ifPresentOrElse()
   * Rewrite this to use an .ifPresentOrElse() instead of this if.
   */
  @Test
  public void testIfPresentOrElse(){
    String nullableString = OptionalTestHelper.getNullableString();
    if(nullableString != null){
      System.out.println(nullableString);
    } else {
      System.out.println("");
    }
  }

  class OptionalTestHelper {
    @Nullable
    public static String getNullableString(){
      return "i could be null";
    }

    public static String getNonNullString(){
      System.out.println("Returning non-null string");
      return "I am not null";
    }

    public static Optional<String> toLowerCase(String text){
      return Optional.of(text.toLowerCase());
    }
  }
}