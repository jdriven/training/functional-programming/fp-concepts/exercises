package nl.jdriven.immutability;

import java.util.Collections;
import java.util.List;

/**
 * In this assignment we will learn to avoid using setters for object creation
 */
public class ImmutabilityAssignment1 {


  /**
   * Part 1: Add a property age (Integer) to Person, notice how the current method flow() person is now in an illegal state and there is no compilation error
   * Part 2: Fix this by removing all setters and using a constructor (or changing Person into a record type)
   */
  public void flow(){
    Person person = new Person();
    person.setName(getName());
    person.setSiblings(getSiblings());
  }


  public String getName(){
    return "Name";
  }

  public List<Person> getSiblings(){
    return Collections.emptyList();
  }


  private class Person {
    private String name;
    private List<Person> siblings;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public List<Person> getSiblings() {
      return siblings;
    }

    public void setSiblings(List<Person> siblings) {
      this.siblings = siblings;
    }
  }

}
