# Getting started

These assignments require JDK 17, Intellij can download this for you  by going to Project structure (f4) -> SDK's and pressing the + icon and select download

## Immutability

In these exercises we will focus on the problems you will encounter and solve when you start using immutability.
In the first exercise we will see what happens if we try to create an object using it's constructor instead of setters.
In the second exercise we will look into changing values of an immutable object
In the third exercise we will see how we can use domain modelling to prevent values from being null

## Streaming

Here we will see how the streaming api will behave as the complexity increases

## Totality

Here we will play with the Optional class to see how this works and how we can properly use it in real life scenarios
First we will do part1 which will you will find in the test directory to get familiar with the Optional 
In part 2 we will look into rewriting a flow using the Optional 

## Purity

Here we will see what effect side-effects have on the testability of our code.
Try to write a test for the functions as-is, and then change the function signature or extract some code to another function to make the function more testable
